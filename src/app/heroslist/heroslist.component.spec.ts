import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroslistComponent } from './heroslist.component';

describe('HeroslistComponent', () => {
  let component: HeroslistComponent;
  let fixture: ComponentFixture<HeroslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroslistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeroslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
