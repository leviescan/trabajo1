import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { Hero } from '../hero.model';
import { ServicioHeroService } from '../servicio-hero.service';

@Component({
  selector: 'app-heroslist',
  templateUrl: './heroslist.component.html',
  styleUrls: ['./heroslist.component.css']
})

export class HeroslistComponent implements OnInit {

  @ViewChild(MatTable) tabla1!: MatTable<Hero>;

  columnas: string[] = ['nombre', 'descripcion', 'url', 'eliminar', 'actualizar'];
  heroes: Hero [] =[];

  constructor( private router:Router, private service:ServicioHeroService) { }

  ngOnInit(): void {
   this.readHeroes();
  }

  readHeroes() {

    return  this.service.getHeroes()
      .subscribe(
        response => {
          this.heroes = response;
        }
      )
  }

  removeHero(id:string){

    this.service.deleteHero(id)
    .subscribe(
      response => {
        this.readHeroes();
      }
    )
  }

}
