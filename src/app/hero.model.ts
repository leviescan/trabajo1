export class Hero {

    _id:  string;
    name: string;
    description: string;
    image: string;
    createdDate?: Date;

    constructor(_idc:string, nombrec:string, descriptionc:string, imagec:string, updated_atc: Date, ){

        this._id= _idc;
        this.name= nombrec;
        this.description= descriptionc;
        this.image= imagec;
        this.createdDate= updated_atc;

    }

}