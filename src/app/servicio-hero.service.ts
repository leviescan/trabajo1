import { Injectable } from '@angular/core';
import { Hero } from './hero.model';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ServicioHeroService {

  restUrl: string = 'http://localhost:3000/crud';

  constructor(private http: HttpClient) { }

  createUser(hero: Hero) {
    throw new Error('Method not implemented.');
  }

  createHero(user: Hero){
    return  this.http.post<Hero>(this.restUrl+'/create', user);
  }

  getHeroes(){
    return  this.http.get<Hero[]>(this.restUrl+'/read');
  }

  getHero(id:string){
   return  this.http.get<Hero>(this.restUrl+'/read/'+ id);
  }

  updateHero(id:string, hero:Hero){
      return  this.http.put<Hero>(this.restUrl + '/' + id, hero);
  }

  deleteHero(id:string){
    return  this.http.delete<Hero>(this.restUrl + '/' + id);
  }

}
