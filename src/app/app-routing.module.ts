import { HeroFormComponent } from './hero-form/hero-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroslistComponent } from './heroslist/heroslist.component';

const routes: Routes = [

  { path: '', component:HeroslistComponent},
  { path: 'heroes', component:HeroslistComponent},
  { path: 'heroes/create', component:HeroFormComponent},
  { path: 'heros/edit/:id', component:HeroFormComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
