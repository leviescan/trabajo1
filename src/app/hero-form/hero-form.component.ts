import { Hero } from './../hero.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicioHeroService } from './../servicio-hero.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit {

  hero: Hero = {
    name: '',
    description: '',
    image: '',
    _id: ''
  }

  constructor(private service: ServicioHeroService, private router: Router, private activatedRoute: ActivatedRoute) { }

  edit: boolean = false;

  ngOnInit(): void {

    const params = this.activatedRoute.snapshot.params
    if (params) {
      this.service.getHero(params['id'])
        .subscribe(
          response => {
            this.hero = response;
            this.edit = true
          },
         
        );

    }
  }

  submitHero() {

    this.service.createHero(this.hero)
      .subscribe(
        response => {
          this.router.navigate(['/'])
        },  
      );
  }

  updateHero() {

    delete this.hero.createdDate;
    this.service.updateHero(this.hero._id, this.hero)
      .subscribe(
        response => {
          this.router.navigate(['/'])
        },
      );
  }

}
